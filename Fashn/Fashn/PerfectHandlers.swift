/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     PerfectHandlers.swift
Description:  Swift Server
Project:      Fashn
Template: /PerfectSwift/server/ServerModuleInit.swift.vmg
 */

import PerfectLib

public func PerfectServerModuleInit() {
	
	PageHandlerRegistry.addPageHandler("IndexHandler") {
		
		// This closure is called in order to create the handler object.
		// It is called once for each relevant request.
		// The supplied WebResponse object can be used to tailor the return value.
		// However, all request processing should take place in the `valuesForResponse` function.
		(r:WebResponse) -> PageHandler in
		
		return IndexHandler()
	}
	
	
	Routing.Handler.registerGlobally()
	Routing.Routes["GET", ["/assets/*/*"]] = { _ in return StaticFileHandler() }
	Routing.Routes["GET", ["/uploads/*"]] = { _ in return StaticFileHandler() }
  	
  	
	//Routes for Asset
	Routing.Routes["GET", ["/api/asset"] ] = { (_:WebResponse) in return AssetListHandler() }
	Routing.Routes["POST", ["/api/asset"] ] = { (_:WebResponse) in return AssetCreateHandler() }
	Routing.Routes["GET", "/api/asset/{id}"] = { _ in return AssetRetrieveHandler() }
	Routing.Routes["PUT", "/api/asset/{id}"] = { _ in return AssetUpdateHandler() }
	Routing.Routes["POST", "/api/asset/{id}"] = { _ in return AssetUpdateHandler() }
	Routing.Routes["DELETE", "/api/asset/{id}"] = { _ in return AssetDeleteHandler() }
	
	//Routes for Composition
	Routing.Routes["GET", ["/api/composition"] ] = { (_:WebResponse) in return CompositionListHandler() }
	Routing.Routes["POST", ["/api/composition"] ] = { (_:WebResponse) in return CompositionCreateHandler() }
	Routing.Routes["GET", "/api/composition/{id}"] = { _ in return CompositionRetrieveHandler() }
	Routing.Routes["PUT", "/api/composition/{id}"] = { _ in return CompositionUpdateHandler() }
	Routing.Routes["POST", "/api/composition/{id}"] = { _ in return CompositionUpdateHandler() }
	Routing.Routes["DELETE", "/api/composition/{id}"] = { _ in return CompositionDeleteHandler() }
	
	//Routes for Image
	Routing.Routes["GET", ["/api/image"] ] = { (_:WebResponse) in return ImageListHandler() }
	Routing.Routes["POST", ["/api/image"] ] = { (_:WebResponse) in return ImageCreateHandler() }
	Routing.Routes["GET", "/api/image/{id}"] = { _ in return ImageRetrieveHandler() }
	Routing.Routes["PUT", "/api/image/{id}"] = { _ in return ImageUpdateHandler() }
	Routing.Routes["POST", "/api/image/{id}"] = { _ in return ImageUpdateHandler() }
	Routing.Routes["DELETE", "/api/image/{id}"] = { _ in return ImageDeleteHandler() }
	
	//Routes for ImageCollection
	Routing.Routes["GET", ["/api/imageCollection"] ] = { (_:WebResponse) in return ImageCollectionListHandler() }
	Routing.Routes["POST", ["/api/imageCollection"] ] = { (_:WebResponse) in return ImageCollectionCreateHandler() }
	Routing.Routes["GET", "/api/imageCollection/{id}"] = { _ in return ImageCollectionRetrieveHandler() }
	Routing.Routes["PUT", "/api/imageCollection/{id}"] = { _ in return ImageCollectionUpdateHandler() }
	Routing.Routes["POST", "/api/imageCollection/{id}"] = { _ in return ImageCollectionUpdateHandler() }
	Routing.Routes["DELETE", "/api/imageCollection/{id}"] = { _ in return ImageCollectionDeleteHandler() }
	
	//Routes for User
	Routing.Routes["GET", ["/api/user"] ] = { (_:WebResponse) in return UserListHandler() }
	Routing.Routes["POST", ["/api/user"] ] = { (_:WebResponse) in return UserCreateHandler() }
	Routing.Routes["GET", "/api/user/{id}"] = { _ in return UserRetrieveHandler() }
	Routing.Routes["PUT", "/api/user/{id}"] = { _ in return UserUpdateHandler() }
	Routing.Routes["POST", "/api/user/{id}"] = { _ in return UserUpdateHandler() }
	Routing.Routes["DELETE", "/api/user/{id}"] = { _ in return UserDeleteHandler() }
	
	print("\(Routing.Routes.description)") 
	//Initialize PM and repositories  
	PersistenceManagerMySQL.sharedInstance
}


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 37.39 minutes to type the 3739+ characters in this file.
 */


