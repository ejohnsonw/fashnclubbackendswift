/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     ImageRepositoryMySQL.swift
Description:  Persistence code for for Image
Project:      Fashn
Template: /PerfectSwift/server/EntityRepositoryMySQL.swift.vm
 */


import MySQL
class ImageRepositoryMySQL : RepositoryMySQL {
func createTable() throws ->  Int {
   let rs = try db.query("CREATE TABLE IF NOT EXISTS Image (description VARCHAR(255), id BIGINT(20) NOT NULL AUTO_INCREMENT, path VARCHAR(255), title VARCHAR(255), PRIMARY KEY (id))")
   let errorCode = db.errorCode()
        if errorCode > 0 {
            throw RepositoryError.CreateTable(errorCode)
      }
      return 0;
}
func insert(entity: Image) throws -> Int {
       	let sql = "INSERT INTO Image(description,path,title) VALUES ( ?, ?, ?)"
       	let db = PersistenceManagerMySQL.sharedInstance.connect();
       	let statement = MySQLStmt(db)
		defer {
			statement.close()
			db.close();
		}
		let prepRes = statement.prepare(sql)
		if(prepRes){

		if(entity.description != nil){
			statement.bindParam(entity.description)
		}else{
			statement.bindParam()
		}
		

if(entity.imageCollection != nil){
			statement.bindParam(entity.imageCollection.id)
}else{
			statement.bindParam()
}
		

		if(entity.path != nil){
			statement.bindParam(entity.path)
		}else{
			statement.bindParam()
		}
		

		if(entity.title != nil){
			statement.bindParam(entity.title)
		}else{
			statement.bindParam()
		}
		

			let execRes = statement.execute()
			if(execRes){
				entity.id = Int(statement.insertId()) ;
				return entity.id
			}else{
				print("\(statement.errorCode()) \(statement.errorMessage()) - \(db.errorCode()) \(db.errorMessage())")
				let errorCode = db.errorCode()
				if errorCode > 0 {
				    throw RepositoryError.Insert(errorCode)
				}
			}
				
			statement.close()
	}        
 	return 0
	}
    
	func update(entity: Image) throws -> Int {
        guard let id = entity.id else {
            return 0
        }
        
        let sql = "UPDATE Image SET description= ? ,path= ? ,title= ? WHERE id = ?"
PersistenceManagerMySQL.sharedInstance.checkConnection();
let statement = MySQLStmt(db)
		defer {
			statement.close()
		}
		let prepRes = statement.prepare(sql)
		
		if(prepRes){		

		if(entity.description != nil){
			statement.bindParam(entity.description)
		}else{
			statement.bindParam()
		}
		

if(entity.imageCollection != nil){
			statement.bindParam(entity.imageCollection.id)
}else{
			statement.bindParam()
}
		

		if(entity.path != nil){
			statement.bindParam(entity.path)
		}else{
			statement.bindParam()
		}
		

		if(entity.title != nil){
			statement.bindParam(entity.title)
		}else{
			statement.bindParam()
		}
		
			statement.bindParam(entity.id)
			let execRes = statement.execute()
			if(!execRes){
				print("\(statement.errorCode()) \(statement.errorMessage()) - \(db.errorCode()) \(db.errorMessage())")
				let errorCode = db.errorCode()
				if errorCode > 0 {
				    throw RepositoryError.Update(errorCode)
				}
			}
	
			statement.close()
		}
        
		return 0
    }
    
	func delete(id: Int) throws -> Int {
	PersistenceManagerMySQL.sharedInstance.checkConnection();
	    let sql = "DELETE FROM image WHERE id = \(id)"
	    let queryResult = db.query(sql)
	    return id;
	}
    
    func retrieve(id: Int) throws -> Image? {
        let sql = "SELECT description,id,path,title FROM Image WHERE id = \(id)"
        PersistenceManagerMySQL.sharedInstance.checkConnection();
		let queryResult = db.query(sql)
		 if(queryResult){
        let results = db.storeResults()!
  		let image = Image()
        while let row = results.next() {
				image.description = String(row[0]);
	
	image.id = Int(row[1]);
	
	image.path = String(row[2]);
	
	image.title = String(row[3]);
	
            print(row)
        }
        results.close()
	    return image;
	    }else{
				print(" \(db.errorCode()) \(db.errorMessage())")
				let errorCode = db.errorCode()
				if errorCode > 0 {
				    throw RepositoryError.Retrieve(errorCode)
				}
			}
			return nil;
    }
    
    func list() throws -> [Image]  {
        let sql = "SELECT description,id,path,title FROM Image "
        PersistenceManagerMySQL.sharedInstance.checkConnection();
        var entities = [Image]()
         let queryResult = db.query(sql)
        if(queryResult){

		let results = db.storeResults()!
  
        while let row = results.next() {
        	let image = Image()
				image.description = String(row[0]);
	
	image.id = Int(row[1]);
	
	image.path = String(row[2]);
	
	image.title = String(row[3]);
	
			entities.append(image)
            print(row)
        }
        results.close()
        return entities
			}else{
				print(" \(db.errorCode()) \(db.errorMessage())")
				let errorCode = db.errorCode()
				if errorCode > 0 {
				    throw RepositoryError.List(errorCode)
				}
				return [];
			}
			
    }
}

/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 48.14 minutes to type the 4814+ characters in this file.
 */


