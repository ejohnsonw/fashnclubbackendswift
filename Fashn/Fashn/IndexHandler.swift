/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     IndexHandler.swift
Description:  Swift Server
Project:      Fashn
Template: /PerfectSwift/server/IndexHandler.swift.vmg
 */

import PerfectLib

class IndexHandler: PageHandler { 

 func valuesForResponse(context: MustacheEvaluationContext, collector: MustacheEvaluationOutputCollector) throws -> MustacheEvaluationContext.MapType {
		
		var values = [String:Any]()
		
		if let acceptStr = context.webRequest?.httpAccept() {
			if acceptStr.contains("json") {
				values["json"] = true
			}
		}
		values["name"] = "Fashn"
		var routes =  Array<String>()
		
		//Routes for Asset
		routes.append("GET /api/asset")
		routes.append("POST /api/asset")
		routes.append("GET /api/asset/{id}")
		routes.append("PUT /api/asset/{id}")
		routes.append("POST /api/asset/{id}")
		routes.append("DELETE /api/asset/{id}")
		//Routes for Composition
		routes.append("GET /api/composition")
		routes.append("POST /api/composition")
		routes.append("GET /api/composition/{id}")
		routes.append("PUT /api/composition/{id}")
		routes.append("POST /api/composition/{id}")
		routes.append("DELETE /api/composition/{id}")
		//Routes for Image
		routes.append("GET /api/image")
		routes.append("POST /api/image")
		routes.append("GET /api/image/{id}")
		routes.append("PUT /api/image/{id}")
		routes.append("POST /api/image/{id}")
		routes.append("DELETE /api/image/{id}")
		//Routes for ImageCollection
		routes.append("GET /api/imageCollection")
		routes.append("POST /api/imageCollection")
		routes.append("GET /api/imageCollection/{id}")
		routes.append("PUT /api/imageCollection/{id}")
		routes.append("POST /api/imageCollection/{id}")
		routes.append("DELETE /api/imageCollection/{id}")
		//Routes for User
		routes.append("GET /api/user")
		routes.append("POST /api/user")
		routes.append("GET /api/user/{id}")
		routes.append("PUT /api/user/{id}")
		routes.append("POST /api/user/{id}")
		routes.append("DELETE /api/user/{id}")

		values["routes"] = routes;
		return values
	}
	
}


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 18.98 minutes to type the 1898+ characters in this file.
 */


