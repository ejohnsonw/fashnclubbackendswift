/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     UserHandlers.swift
Description:  User Handlers for REST endpoints 
Project:      Fashn
Template: /PerfectSwift/server/entityHandlerClass.swift.vm
 */

import PerfectLib

class UserListHandler: RequestHandler  {
  
  func handleRequest(request: WebRequest, response: WebResponse) {
  do{
  	let users : [User]  = try PersistenceManagerMySQL.sharedInstance.userRepository.list()
  	print (NSJSONSerialization.isValidJSONObject (users ))
  	
        let json = try User.encodeList(users );
        try response.outputJson(json)
  	}catch{
  	  response.setStatus (500, message: "Could not list User data")
  	}
    //response.appendBodyString("Index handler: You accessed path \(request.requestURI())")
    response.requestCompletedCallback()
  }
}

class UserCreateHandler: RequestHandler {
  func handleRequest(request: WebRequest, response: WebResponse) {
     let user = User() 
     do {
    	try user.decode(request.postBodyString);
    	let result = try PersistenceManagerMySQL.sharedInstance.userRepository.insert(user)
    	let json = try user.encode()
    	try response.outputJson(json)
    }catch{
        response.appendBodyString("Error accessing data:  \(error)")
    }
    response.requestCompletedCallback()
  }
}

class UserRetrieveHandler: RequestHandler {
  func handleRequest(request: WebRequest, response: WebResponse) {
   let id = Int(request.urlVariables["id"]!)
    do{
        let user : User  = try PersistenceManagerMySQL.sharedInstance.userRepository.retrieve(id!)!
        let json = try user.encode()
        try response.outputJson(json)
    }catch{
        response.setStatus (500, message: "Could not retrieve User \(id) data")
    }
    response.requestCompletedCallback()
  }
}

class UserUpdateHandler: RequestHandler {
  func handleRequest(request: WebRequest, response: WebResponse) {
    do {
     	let user = User() 
    	try user.decode(request.postBodyString);
    	let result = try PersistenceManagerMySQL.sharedInstance.userRepository.update(user)
    	let json = try user.encode()
    	try response.outputJson(json)
    }catch{
        response.appendBodyString("Error accessing data:  \(error)")
    }
    response.requestCompletedCallback()
  }
}

class UserDeleteHandler: RequestHandler {
  func handleRequest(request: WebRequest, response: WebResponse) {
    let id = Int(request.urlVariables["id"]!)
    do{
        let result = try PersistenceManagerMySQL.sharedInstance.userRepository.delete(id!)
        //let json = try user.encode()
        try response.outputJson("{\"id\":\(id),\"message\":\"deleted\"}")
    }catch{
        response.setStatus (500, message: "Could not delete User \(id) data")
    }
    response.requestCompletedCallback()
  }
}

/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 26.26 minutes to type the 2626+ characters in this file.
 */


