/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     AssetRepositoryMySQL.swift
Description:  Persistence code for for Asset
Project:      Fashn
Template: /PerfectSwift/server/EntityRepositoryMySQL.swift.vm
 */


import MySQL
class AssetRepositoryMySQL : RepositoryMySQL {
func createTable() throws ->  Int {
   let rs = try db.query("CREATE TABLE IF NOT EXISTS Asset (id BIGINT(20) NOT NULL AUTO_INCREMENT, x FLOAT, y FLOAT, z FLOAT, PRIMARY KEY (id))")
   let errorCode = db.errorCode()
        if errorCode > 0 {
            throw RepositoryError.CreateTable(errorCode)
      }
      return 0;
}
func insert(entity: Asset) throws -> Int {
       	let sql = "INSERT INTO Asset(x,y,z) VALUES ( ?, ?, ?)"
       	let db = PersistenceManagerMySQL.sharedInstance.connect();
       	let statement = MySQLStmt(db)
		defer {
			statement.close()
			db.close();
		}
		let prepRes = statement.prepare(sql)
		if(prepRes){

if(entity.composition != nil){
			statement.bindParam(entity.composition.id)
}else{
			statement.bindParam()
}
		

if(entity.image != nil){
			statement.bindParam(entity.image.id)
}else{
			statement.bindParam()
}
		

		if(entity.x != nil){
			statement.bindParam(entity.x)
		}else{
			statement.bindParam()
		}
		

		if(entity.y != nil){
			statement.bindParam(entity.y)
		}else{
			statement.bindParam()
		}
		

		if(entity.z != nil){
			statement.bindParam(entity.z)
		}else{
			statement.bindParam()
		}
		

			let execRes = statement.execute()
			if(execRes){
				entity.id = Int(statement.insertId()) ;
				return entity.id
			}else{
				print("\(statement.errorCode()) \(statement.errorMessage()) - \(db.errorCode()) \(db.errorMessage())")
				let errorCode = db.errorCode()
				if errorCode > 0 {
				    throw RepositoryError.Insert(errorCode)
				}
			}
				
			statement.close()
	}        
 	return 0
	}
    
	func update(entity: Asset) throws -> Int {
        guard let id = entity.id else {
            return 0
        }
        
        let sql = "UPDATE Asset SET x= ? ,y= ? ,z= ? WHERE id = ?"
PersistenceManagerMySQL.sharedInstance.checkConnection();
let statement = MySQLStmt(db)
		defer {
			statement.close()
		}
		let prepRes = statement.prepare(sql)
		
		if(prepRes){		

if(entity.composition != nil){
			statement.bindParam(entity.composition.id)
}else{
			statement.bindParam()
}
		

if(entity.image != nil){
			statement.bindParam(entity.image.id)
}else{
			statement.bindParam()
}
		

		if(entity.x != nil){
			statement.bindParam(entity.x)
		}else{
			statement.bindParam()
		}
		

		if(entity.y != nil){
			statement.bindParam(entity.y)
		}else{
			statement.bindParam()
		}
		

		if(entity.z != nil){
			statement.bindParam(entity.z)
		}else{
			statement.bindParam()
		}
		
			statement.bindParam(entity.id)
			let execRes = statement.execute()
			if(!execRes){
				print("\(statement.errorCode()) \(statement.errorMessage()) - \(db.errorCode()) \(db.errorMessage())")
				let errorCode = db.errorCode()
				if errorCode > 0 {
				    throw RepositoryError.Update(errorCode)
				}
			}
	
			statement.close()
		}
        
		return 0
    }
    
	func delete(id: Int) throws -> Int {
	PersistenceManagerMySQL.sharedInstance.checkConnection();
	    let sql = "DELETE FROM asset WHERE id = \(id)"
	    let queryResult = db.query(sql)
	    return id;
	}
    
    func retrieve(id: Int) throws -> Asset? {
        let sql = "SELECT id,x,y,z FROM Asset WHERE id = \(id)"
        PersistenceManagerMySQL.sharedInstance.checkConnection();
		let queryResult = db.query(sql)
		 if(queryResult){
        let results = db.storeResults()!
  		let asset = Asset()
        while let row = results.next() {
				asset.id = Int(row[0]);
	
	asset.x = Float(row[1]);
	
	asset.y = Float(row[2]);
	
	asset.z = Float(row[3]);
	
            print(row)
        }
        results.close()
	    return asset;
	    }else{
				print(" \(db.errorCode()) \(db.errorMessage())")
				let errorCode = db.errorCode()
				if errorCode > 0 {
				    throw RepositoryError.Retrieve(errorCode)
				}
			}
			return nil;
    }
    
    func list() throws -> [Asset]  {
        let sql = "SELECT id,x,y,z FROM Asset "
        PersistenceManagerMySQL.sharedInstance.checkConnection();
        var entities = [Asset]()
         let queryResult = db.query(sql)
        if(queryResult){

		let results = db.storeResults()!
  
        while let row = results.next() {
        	let asset = Asset()
				asset.id = Int(row[0]);
	
	asset.x = Float(row[1]);
	
	asset.y = Float(row[2]);
	
	asset.z = Float(row[3]);
	
			entities.append(asset)
            print(row)
        }
        results.close()
        return entities
			}else{
				print(" \(db.errorCode()) \(db.errorMessage())")
				let errorCode = db.errorCode()
				if errorCode > 0 {
				    throw RepositoryError.List(errorCode)
				}
				return [];
			}
			
    }
}

/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 48.04 minutes to type the 4804+ characters in this file.
 */


