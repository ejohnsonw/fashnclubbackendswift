/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     ImageCollectionHandlers.swift
Description:  ImageCollection Handlers for REST endpoints 
Project:      Fashn
Template: /PerfectSwift/server/entityHandlerClass.swift.vm
 */

import PerfectLib

class ImageCollectionListHandler: RequestHandler  {
  
  func handleRequest(request: WebRequest, response: WebResponse) {
  do{
  	let imageCollections : [ImageCollection]  = try PersistenceManagerMySQL.sharedInstance.imageCollectionRepository.list()
  	print (NSJSONSerialization.isValidJSONObject (imageCollections ))
  	
        let json = try ImageCollection.encodeList(imageCollections );
        try response.outputJson(json)
  	}catch{
  	  response.setStatus (500, message: "Could not list ImageCollection data")
  	}
    //response.appendBodyString("Index handler: You accessed path \(request.requestURI())")
    response.requestCompletedCallback()
  }
}

class ImageCollectionCreateHandler: RequestHandler {
  func handleRequest(request: WebRequest, response: WebResponse) {
     let imageCollection = ImageCollection() 
     do {
    	try imageCollection.decode(request.postBodyString);
    	let result = try PersistenceManagerMySQL.sharedInstance.imageCollectionRepository.insert(imageCollection)
    	let json = try imageCollection.encode()
    	try response.outputJson(json)
    }catch{
        response.appendBodyString("Error accessing data:  \(error)")
    }
    response.requestCompletedCallback()
  }
}

class ImageCollectionRetrieveHandler: RequestHandler {
  func handleRequest(request: WebRequest, response: WebResponse) {
   let id = Int(request.urlVariables["id"]!)
    do{
        let imageCollection : ImageCollection  = try PersistenceManagerMySQL.sharedInstance.imageCollectionRepository.retrieve(id!)!
        let json = try imageCollection.encode()
        try response.outputJson(json)
    }catch{
        response.setStatus (500, message: "Could not retrieve ImageCollection \(id) data")
    }
    response.requestCompletedCallback()
  }
}

class ImageCollectionUpdateHandler: RequestHandler {
  func handleRequest(request: WebRequest, response: WebResponse) {
    do {
     	let imageCollection = ImageCollection() 
    	try imageCollection.decode(request.postBodyString);
    	let result = try PersistenceManagerMySQL.sharedInstance.imageCollectionRepository.update(imageCollection)
    	let json = try imageCollection.encode()
    	try response.outputJson(json)
    }catch{
        response.appendBodyString("Error accessing data:  \(error)")
    }
    response.requestCompletedCallback()
  }
}

class ImageCollectionDeleteHandler: RequestHandler {
  func handleRequest(request: WebRequest, response: WebResponse) {
    let id = Int(request.urlVariables["id"]!)
    do{
        let result = try PersistenceManagerMySQL.sharedInstance.imageCollectionRepository.delete(id!)
        //let json = try imageCollection.encode()
        try response.outputJson("{\"id\":\(id),\"message\":\"deleted\"}")
    }catch{
        response.setStatus (500, message: "Could not delete ImageCollection \(id) data")
    }
    response.requestCompletedCallback()
  }
}

/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 29.78 minutes to type the 2978+ characters in this file.
 */


