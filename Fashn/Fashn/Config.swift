/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     Config.swift
Description:  Server Configuration
Project:      Fashn
Template: /PerfectSwift/server/Config.swift.vmg
 */
import PerfectLib

class Config {
	static let sharedInstance = Config()
	let serverPort = 9000
     let sessionName = "session"
     let sessionExpires = 60
     let uploadDirPath = PerfectServer.staticPerfectServer.homeDir() + "webroot/uploads/"
     let uploadDirUrl = "/uploads/"
   
    
   var datasources = Dictionary<String,GeneratronDataSource>();
    
   init() {
    var mysql = GeneratronDataSource();
    mysql.name = "fashnmyql"
    mysql.host = "localhost"
    mysql.user = "fashn"
    mysql.password = "fashn123"
    mysql.schema = "Fashn"
    datasources["mysql"] = mysql;
    
    var mongodb = GeneratronDataSource();
    mongodb.name = "fashnmongo"
    mongodb.host = "localhost"
    mongodb.user = "fashn"
    mongodb.password = "fashn123"
    mongodb.schema = "Fashn"
    datasources["mongodb"] = mongodb;
    
    }
    
  
    
}


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 8.88 minutes to type the 888+ characters in this file.
 */


