/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     UserRepositoryMySQL.swift
Description:  Persistence code for for User
Project:      Fashn
Template: /PerfectSwift/server/EntityRepositoryMySQL.swift.vm
 */


import MySQL
class UserRepositoryMySQL : RepositoryMySQL {
func createTable() throws ->  Int {
   let rs = try db.query("CREATE TABLE IF NOT EXISTS User (email VARCHAR(255), id BIGINT(20) NOT NULL AUTO_INCREMENT, name VARCHAR(255), photo VARCHAR(255), PRIMARY KEY (id))")
   let errorCode = db.errorCode()
        if errorCode > 0 {
            throw RepositoryError.CreateTable(errorCode)
      }
      return 0;
}
func insert(entity: User) throws -> Int {
       	let sql = "INSERT INTO User(email,name,photo) VALUES ( ?, ?, ?)"
       	let db = PersistenceManagerMySQL.sharedInstance.connect();
       	let statement = MySQLStmt(db)
		defer {
			statement.close()
			db.close();
		}
		let prepRes = statement.prepare(sql)
		if(prepRes){

		

		if(entity.email != nil){
			statement.bindParam(entity.email)
		}else{
			statement.bindParam()
		}
		

		

		if(entity.name != nil){
			statement.bindParam(entity.name)
		}else{
			statement.bindParam()
		}
		

		if(entity.photo != nil){
			statement.bindParam(entity.photo)
		}else{
			statement.bindParam()
		}
		

			let execRes = statement.execute()
			if(execRes){
				entity.id = Int(statement.insertId()) ;
				return entity.id
			}else{
				print("\(statement.errorCode()) \(statement.errorMessage()) - \(db.errorCode()) \(db.errorMessage())")
				let errorCode = db.errorCode()
				if errorCode > 0 {
				    throw RepositoryError.Insert(errorCode)
				}
			}
				
			statement.close()
	}        
 	return 0
	}
    
	func update(entity: User) throws -> Int {
        guard let id = entity.id else {
            return 0
        }
        
        let sql = "UPDATE User SET email= ? ,name= ? ,photo= ? WHERE id = ?"
PersistenceManagerMySQL.sharedInstance.checkConnection();
let statement = MySQLStmt(db)
		defer {
			statement.close()
		}
		let prepRes = statement.prepare(sql)
		
		if(prepRes){		

		

		if(entity.email != nil){
			statement.bindParam(entity.email)
		}else{
			statement.bindParam()
		}
		

		

		if(entity.name != nil){
			statement.bindParam(entity.name)
		}else{
			statement.bindParam()
		}
		

		if(entity.photo != nil){
			statement.bindParam(entity.photo)
		}else{
			statement.bindParam()
		}
		
			statement.bindParam(entity.id)
			let execRes = statement.execute()
			if(!execRes){
				print("\(statement.errorCode()) \(statement.errorMessage()) - \(db.errorCode()) \(db.errorMessage())")
				let errorCode = db.errorCode()
				if errorCode > 0 {
				    throw RepositoryError.Update(errorCode)
				}
			}
	
			statement.close()
		}
        
		return 0
    }
    
	func delete(id: Int) throws -> Int {
	PersistenceManagerMySQL.sharedInstance.checkConnection();
	    let sql = "DELETE FROM user WHERE id = \(id)"
	    let queryResult = db.query(sql)
	    return id;
	}
    
    func retrieve(id: Int) throws -> User? {
        let sql = "SELECT email,id,name,photo FROM User WHERE id = \(id)"
        PersistenceManagerMySQL.sharedInstance.checkConnection();
		let queryResult = db.query(sql)
		 if(queryResult){
        let results = db.storeResults()!
  		let user = User()
        while let row = results.next() {
				//Is a collection, not supported yet
    //user.compositions = Array(row[0]);

	user.email = String(row[0]);
	
	user.id = Int(row[1]);
	
	//Is a collection, not supported yet
    //user.imageCollections = Array(row[2]);

	user.name = String(row[2]);
	
	user.photo = String(row[3]);
	
            print(row)
        }
        results.close()
	    return user;
	    }else{
				print(" \(db.errorCode()) \(db.errorMessage())")
				let errorCode = db.errorCode()
				if errorCode > 0 {
				    throw RepositoryError.Retrieve(errorCode)
				}
			}
			return nil;
    }
    
    func list() throws -> [User]  {
        let sql = "SELECT email,id,name,photo FROM User "
        PersistenceManagerMySQL.sharedInstance.checkConnection();
        var entities = [User]()
         let queryResult = db.query(sql)
        if(queryResult){

		let results = db.storeResults()!
  
        while let row = results.next() {
        	let user = User()
				//Is a collection, not supported yet
    //user.compositions = Array(row[0]);

	user.email = String(row[0]);
	
	user.id = Int(row[1]);
	
	//Is a collection, not supported yet
    //user.imageCollections = Array(row[2]);

	user.name = String(row[2]);
	
	user.photo = String(row[3]);
	
			entities.append(user)
            print(row)
        }
        results.close()
        return entities
			}else{
				print(" \(db.errorCode()) \(db.errorMessage())")
				let errorCode = db.errorCode()
				if errorCode > 0 {
				    throw RepositoryError.List(errorCode)
				}
				return [];
			}
			
    }
}

/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 48.14 minutes to type the 4814+ characters in this file.
 */


